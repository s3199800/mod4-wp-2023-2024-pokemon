let trainers = {};
let tableParentId = 'table';
let detailsParentId = 'details';

function getRowId(resource) {
    return `${resource.id}_row`
}

function updateTrainerDetails(trainerId) {
    const pt = trainers?.data?.find(item => item.id === trainerId);
    const parent = document.getElementById(detailsParentId);

    parent.innerHTML = `
    <div class="card" id="${pt.id}_card">
      <img src="${pt.profileUrl}" class="card-img-top" alt="${pt.id}">
      <div class="card-body">
      <h5 class="card-title">${pt.name} #${pt.id}</h5>
        </div>
    </div>
    `
}

function trainersToRow(trainer) {
    return `
    <tr id="${getRowId(trainer)}" onclick="updateTrainerDetails('${trainer?.id?.trim()}')">
        <th scope="row">${trainer.id}</th>
        <td>${trainer.name}</td>
        
    </tr>
    `
}

function createTrainerTable() {
    const tableParent = document.getElementById(tableParentId);
    tableParent.innerHTML = `
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">Name</th>  
                    <th scope="col">ID</th>
                    
                </tr>
            </thead>
            <tbody>
                ${
        trainers.data.map(resource => `${trainersToRow(resource)}`).join("\n")
        || "no data"
    }
            </tbody>
        </table>
    `
}